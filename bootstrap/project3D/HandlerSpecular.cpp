#include "HandlerSpecular.h"

HandlerSpecular::HandlerSpecular(float * specular) : NetworkDataHandler(5)
{
	this->specular = specular;
}

HandlerSpecular::~HandlerSpecular()
{
}

void HandlerSpecular::handleData(std::string data, Address * address)
{
	float amount = stof(data);
	if (amount > 1)
	{
		amount = 1;
	}
	else if (amount < 0)
	{
		amount = 0;
	}

	*specular = amount;
}
