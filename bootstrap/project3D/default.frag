#version 410

in vec2 vTexCoord;
in vec3 vNormal;
in vec4 vPosition;

out vec4 fragColor;

uniform sampler2D texture_diffuse1;
uniform vec3 lightDirection;
uniform vec3 lightColor;
uniform vec3 cameraPos;
uniform float specPow;

void main() 
{
	float d = max(0, dot(normalize(vNormal.xyz), lightDirection));
	vec3 E = normalize( cameraPos - vPosition.xyz ); 
	vec3 R = reflect( -lightDirection, vNormal.xyz ); 
	float s = max( 0, dot( E, R ) ); s = pow( s, specPow );

    fragColor = texture(texture_diffuse1, vTexCoord) * vec4(lightColor * d + vec3(0,0,1) * s, 1);
}