#pragma once
#include <NetworkDataHandler.h>

#include <glm\glm.hpp>

class HandlerLightColor : public NetworkDataHandler
{
public:
	HandlerLightColor(glm::vec3* lightColor);
	~HandlerLightColor();

private:
	glm::vec3* lightColor;

	// Inherited via NetworkDataHandler
	virtual void handleData(std::string data, Address * address) override;
};

