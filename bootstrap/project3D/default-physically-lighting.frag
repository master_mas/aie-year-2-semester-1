#version 410

in vec2 vTexCoord;
in vec3 vNormal;
in vec4 vPosition;
in vec3 vTangent; 
in vec3 vBiTangent;

out vec4 fragColor;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_normal1;
uniform sampler2D texture_specular1;
uniform vec3 lightDirection;
uniform vec3 lightColor;
uniform vec3 cameraPos;
uniform float specPow;
uniform float roughness;

void main() 
{
	//vec3 normal = texture(texture_normal1, vTexCoord).rgb;
	//normal = normalize(normal * 2.0 - 1.0);

	//normal = normalize(TBN * normal);

	vec3 E = normalize( cameraPos - vPosition.xyz ); 
	float R2 = roughness * roughness;
	float A = 1.0f - 0.5f * R2 / (R2 + 0.33f);
	float B = 0.45f * R2 / (R2 + 0.09f);

	float NdL = max(0.0f, dot(vNormal, lightDirection));
	float NdE = max(0.0f, dot(vNormal, E));

	vec3 lightProjected = normalize(lightDirection - vNormal * NdL);
	vec3 viewProjected = normalize(E - vNormal * NdE);
	float CX = max(0.0f, dot(lightProjected, viewProjected));

	float alpha = sin(max(acos(NdE), acos(NdL)));
	float beta = tan(min(acos(NdE), acos(NdL)));
	float DX = alpha * beta;

	float orenLayer = NdL * (A + B * CX * DX);

	//Specular
	vec3 R = reflect( -lightDirection, vNormal.xyz ); 
	float s = max( 0, dot( E, R ) ); 
	s = pow( s, specPow );

    fragColor = texture(texture_diffuse1, vTexCoord) * vec4(lightColor * orenLayer + vec3(1,1,1) * (texture(texture_specular1, vTexCoord).xyz * s), 1);
}