#pragma once
#include "Shader.h"
#include "Camera.h"
class PostProcessing
{
public:
	//Structors
	PostProcessing();
	~PostProcessing();

	//Public Variables
	unsigned int m_fbo;

	//Public Functions
	void draw(Shader* shader, Camera* camera);

private:
	//Private Variables
	unsigned int m_fboTexture;
	unsigned int m_fboDepth;

	unsigned int m_vbo;
	unsigned int m_vao;
};

