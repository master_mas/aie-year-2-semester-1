#pragma once
#include "NetworkDataHandler.h"
#include <NetworkDataHandler.h>
#include <glm\glm.hpp>
class HandlerLightDirection :
	public NetworkDataHandler
{
public:
	HandlerLightDirection(float* direction);
	~HandlerLightDirection();

	// Inherited via NetworkDataHandler
	virtual void handleData(std::string data, Address * address) override;

private:
	float* direction;
};

