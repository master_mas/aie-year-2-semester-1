#pragma once
#include <vector>
#include "Shader.h"

class ShaderLoader
{
public:
	ShaderLoader();
	~ShaderLoader();

	Shader* getShader(unsigned int shader);

	//Shaders
	static const unsigned int SHADER_TEXTURE_WITH_LIGHTING = 0;
	static const unsigned int SHADER_PARTICLE = 1;
	static const unsigned int SHADER_POST_PROCESS = 2;
private:
	std::vector<Shader*> shaders;
	void add(const char* vertex, const char* fragment);
};

