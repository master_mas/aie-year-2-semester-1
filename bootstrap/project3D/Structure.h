#pragma once
#include <glm/glm.hpp>
#include <string>

class Structure
{
public:
	//Structors
	Structure() {}
	~Structure() {}

	//Structs
	struct Position
	{
		glm::vec4 position;
	};

	struct Vertex {
		glm::vec4 position;
		glm::vec4 tangants;
		glm::vec3 normal;
		glm::vec2 texCoord;
	};

	struct Texture
	{
		unsigned int id;
		std::string m_type;
		std::string m_path;
	};


	struct VertexOld
	{
		glm::vec4 position;
		glm::vec4 colour;
		glm::vec2 texCoord;
	};

	struct Particle
	{
		glm::vec3 position;
		glm::vec3 velocity;
		glm::vec4 color;
		float size;
		float lifeTime;
		float lifeSpan;
	};

	struct ParticleVertex
	{
		glm::vec4 position;
		glm::vec4 color;
	};
};

