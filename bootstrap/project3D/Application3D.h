#pragma once
#include "Application.h"
#include "ModelSoulSpear.h"
#include "ParticleEmitter.h"
#include "PostProcessing.h"
#include "FlyCamera.h"
#include <NetworkServer.h>

class Application3D : public aie::Application {
public:

	Application3D();
	virtual ~Application3D();

	//Override Functions
	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	void addEntity(Entity* entity);

protected:
	//Variables
	Camera* m_camera = nullptr;
	PostProcessing* m_postProcessing = nullptr;

private:
	//Variables for UI
	glm::vec4 m_clearColour = glm::vec4(0.25f, 0.25f, 0.25f, 1);
	float m_lightDirection[3] = { 0, 0, -1 };
	glm::vec3 m_lightColor = glm::vec3(1, 1, 1);
	float m_specularPower = 0.9f;
	float m_roughness = 0.01f;
	int m_totalRender = 0;

	std::vector<Entity*> entities;
	ShaderLoader* shaderLoader;
	vec4 m_planes[6];

	NetworkServer* networkClient = nullptr;
};