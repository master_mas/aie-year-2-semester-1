#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "..\dependencies\glfw\include\GLFW\glfw3.h"

class Component
{
public:
	Component();
	~Component();

	virtual void update(float deltaTime) = 0;

private:

};

