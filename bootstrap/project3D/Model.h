#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "Shader.h"
#include "Mesh.h"

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

#include <stb_image.h>
#include <memory>

#include "Texture2D.h"
#include "BoundingSphere.h"
#include "Entity.h"

class Model : public Entity
{
public:
	//Structors
	Model(Model* takeIn);
	Model(const aiScene* scene, std::shared_ptr<std::vector<Texture2D*>> textures);
	~Model();

	//Public Functions
	void draw(Camera* camera);
	void setLightDirection(glm::vec3 direction);
	void setLightColor(glm::vec3 color);
	void setSpecularPower(float power);
	void setRoughness(float m_roughness);
	std::vector<glm::vec3> getVBOPoints();

	//Public Variables
	const aiScene* scene;

private:
	//Private Variables
	std::vector<Mesh> m_meshes;
	std::string m_directory;
	std::shared_ptr<std::vector<Texture2D*>> m_textures_loaded;

	//Private Functions
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
};

