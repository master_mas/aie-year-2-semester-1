#pragma once
#include <vector>
#include <glm\glm.hpp>
class AABB
{
public:
	AABB();
	~AABB();

	void reset();
	void fit(const std::vector<glm::vec3>& points);

private:
	glm::vec3 min, max;
};

