#include "BatchedModelLoader.h"

BatchedModelLoader::BatchedModelLoader(char * address, unsigned int totalBatch)
{
	const aiScene* scene = loadModelData(address);
	if (scene == nullptr)
	{
		return;
	}

	textures = std::make_shared<std::vector<Texture2D*>>();
	nodeExtractor(scene->mRootNode, scene);

	for (unsigned int i = 0; i < totalBatch; ++i)
	{
		Model* model = new Model(scene, textures);
		models.push_back(model);
	}
}

BatchedModelLoader::~BatchedModelLoader()
{
}

std::vector<Model*>& BatchedModelLoader::getModels()
{
	return models;
}

const aiScene* BatchedModelLoader::loadModelData(char * address)
{
	const aiScene* scene = importer.ReadFile(address, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "ERROR::ASSIMP::" << importer.GetErrorString() << std::endl;
		return nullptr;
	}

	std::string temp = std::string(address);
	this->directory = temp.substr(0, temp.find_last_of('/'));

	return scene;
}

void BatchedModelLoader::loadMaterialTextures(aiMaterial * mat, aiTextureType m_type, std::string typeName)
{
	for (unsigned int i = 0; i < mat->GetTextureCount(m_type); ++i)
	{
		std::cout << "Loading Texture: " << typeName << " : " << directory << std::endl;
		aiString str;
		mat->GetTexture(m_type, i, &str);
		bool skip = false;
		for (unsigned int j = 0; j < textures->size(); j++)
		{
			if (std::strcmp(textures->at(j)->getPath().c_str(), str.C_Str()) == 0)
			{
				skip = true;
				break;
			}
		}
		if (!skip)
		{   // If texture hasn't been loaded already, load it
			Texture2D* texture = new Texture2D();
			texture->loadTexture((this->directory + '/' + str.C_Str()).c_str(), typeName.c_str());
			textures->push_back(texture);
		}
	}
}

void BatchedModelLoader::nodeExtractor(aiNode * node, const aiScene* scene)
{
	for (unsigned int i = 0; i < node->mNumMeshes; ++i)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		if (mesh->mMaterialIndex >= 0)
		{
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
			loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
			loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_diffuse");
			loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_diffuse");
		}
	}

	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		nodeExtractor(node->mChildren[i], scene);
	}
}
