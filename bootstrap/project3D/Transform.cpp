#include "..\project2D\Transform.h"
#include "..\project2D\Transform.h"
#include "..\project2D\Transform.h"
#include "Transform.h"

Transform::Transform()
{
}

Transform::~Transform()
{
}

glm::vec3 Transform::getPosition()
{
	return glm::vec3(transform[3][0], transform[3][1], transform[3][2]);
}

void Transform::setPosition(glm::vec3 position)
{
	transform = glm::translate(transform, position);
}

void Transform::rotate(glm::vec3 axis, float angle)
{
	transform = glm::rotate(transform, angle, axis);
}

void Transform::translate(glm::vec3 pos)
{
	transform = glm::translate(transform, pos);
}

glm::mat4 * Transform::getTransform()
{
	return &transform;
}

void Transform::update(float deltaTime)
{
}
