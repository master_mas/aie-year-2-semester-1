#pragma once
#include <NetworkDataHandler.h>
#include <vector>
#include "Model.h"

class HandlerRoughness : public NetworkDataHandler
{
public:
	HandlerRoughness(float* m_roughness);
	~HandlerRoughness();

	// Inherited via NetworkDataHandler
	virtual void handleData(std::string data, Address * address) override;

private:
	float* roughness;
};

