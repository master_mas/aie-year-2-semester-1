#include "HandlerLightDirection.h"
#include <vector>
#include <string>
#include <iterator>
#include <sstream>

HandlerLightDirection::HandlerLightDirection(float* direction) : NetworkDataHandler(4)
{
	this->direction = direction;
}

HandlerLightDirection::~HandlerLightDirection()
{
}

void HandlerLightDirection::handleData(std::string data, Address * address)
{
	std::istringstream iss(data);
	std::vector<std::string> tokens{ std::istream_iterator<std::string>{iss},
		std::istream_iterator<std::string>{} };

	if (tokens.size() == 3)
	{
		float x = stof(tokens[0]);
		float y = stof(tokens[1]);
		float z = stof(tokens[2]);

		direction[0] = x;
		direction[1] = y;
		direction[2] = z;
	}
}
