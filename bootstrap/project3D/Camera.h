#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <imgui_glfw3.h>
#include "..\dependencies\glfw\include\GLFW\glfw3.h"

using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

class Camera
{
public:
	//Structors
	Camera();
	virtual ~Camera();

	//Override Update
	virtual void update(float deltaTime, GLFWwindow* window) = 0;

	//Public Functions
	void setPerspective(float fieldOfView, float aspectRatio, float near, float far);
	void setLookAt(vec3 from, vec3 to, vec3 m_up);
	void setPosition(const vec3 & position);
	void setWorldTransform(const glm::mat4& m_transform);
	vec3 getPosition();
	vec3 getRow(int row);
	void getFrustumPlanes(glm::vec4 * m_planes);

	//Public Variables
	mat4& getWorldTransform();
	mat4& getView();
	mat4& getProjection();
	mat4& getProjectionView();

private:
	//Private Variables
	mat4 m_worldTransform;
	mat4 m_viewTransform;
	mat4 m_projectionTransform;
	mat4 m_projectionViewTransform;

	//Private Functions
	void updateProjectionView();
	void updateWorldTransform();
	void updateViewTransform();
};

