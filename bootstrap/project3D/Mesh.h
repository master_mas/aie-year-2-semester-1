#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>

#include "Structure.h"
#include <assimp\Importer.hpp>
#include "Shader.h"
#include "Camera.h"
#include "Texture2D.h"

#include <vector>
#include <sstream>
#include <memory>

class Mesh
{
public:
	//Public Variables
	std::vector<Structure::Vertex> m_vertices;
	std::vector<GLuint> m_indices;
	std::shared_ptr<std::vector<Texture2D*>> m_textures;

	//Structors
	Mesh(std::vector<Structure::Vertex> m_vertices, std::vector<GLuint> m_indices, std::shared_ptr<std::vector<Texture2D*>> m_textures);
	~Mesh();
	void draw(Shader* shader, Camera *camera, glm::mat4 & position);

	//Public Functions
	void setLightDirection(glm::vec3 direction);
	void setLightColor(glm::vec3 color);
	void setSpecularPower(float power);
	void setRoughness(float m_roughness);

private:
	//Private Variables
	GLuint VAO, VBO, EBO;
	
	glm::vec3 m_lightDirection = glm::vec3(0, 0, -1);
	glm::vec3 m_lightColor = glm::vec3(1, 1, 1);
	float m_specularPower = 0.9f;
	float m_roughness = 0.01f;

	//Private Functions
	void setupMesh();
};

