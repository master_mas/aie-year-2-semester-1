#include "Camera.h"

Camera::Camera()
{
	m_worldTransform = mat4();
	m_viewTransform = mat4();
	m_projectionTransform = mat4();
	m_projectionViewTransform = mat4();
}

Camera::~Camera()
{
}

void Camera::setPerspective(float fieldOfView, float aspectRatio, float near, float far)
{
	m_projectionTransform = mat4(
		1.0f / (aspectRatio * tan(fieldOfView / 2)), 0.0f, 0.0f, 0.0f, 
		0.0f, 1.0f / (tan(fieldOfView / 2)), 0.0f, 0.0f,
		0.0f, 0.0f, -((far + near) / (far - near)), -1.0f,
		0.0f, 0.0f, -((2 * (far * near)) / (far - near)), 0.0f);

	//m_projectionTransform = glm::perspective(fieldOfView, aspectRatio, near, far);
}

void Camera::setLookAt(vec3 from, vec3 to, vec3 m_up)
{
	m_viewTransform = glm::lookAt(from, to, m_up);
	updateWorldTransform();
	updateProjectionView();
}

void Camera::setPosition(const vec3 & position)
{
	//worldTransfrom[3] = vec4(position, 1);

	m_worldTransform[3][0] = position.x;
	m_worldTransform[3][1] = position.y;
	m_worldTransform[3][2] = position.z;
	updateViewTransform();
	updateProjectionView();
	//updateWorldTransfrom();
}

mat4 & Camera::getWorldTransform()
{
	return m_worldTransform;
}

mat4 & Camera::getView()
{
	return m_viewTransform;
}

mat4 & Camera::getProjection()
{
	return m_projectionTransform;
}

mat4 & Camera::getProjectionView()
{
	return m_projectionViewTransform;
}

void Camera::setWorldTransform(const glm::mat4& m_transform)
{
	m_worldTransform = m_transform;
	m_viewTransform = glm::inverse(m_worldTransform);
	updateProjectionView();
	updateWorldTransform();
}

vec3 Camera::getPosition()
{
	return getRow(3);
}

vec3 Camera::getRow(int row)
{
	return vec3(m_worldTransform[row].x, m_worldTransform[row].y, m_worldTransform[row].z);
}

void Camera::updateProjectionView()
{
	m_projectionViewTransform = m_projectionTransform * m_viewTransform;
}

void Camera::updateWorldTransform()
{
	m_worldTransform = glm::inverse(m_viewTransform);
}

void Camera::updateViewTransform()
{
	m_viewTransform = glm::inverse(m_worldTransform);
}

void Camera::getFrustumPlanes(glm::vec4* m_planes) {
	// right side
	const glm::mat4 m_transform = getProjectionView();

	m_planes[0] = vec4(
		m_transform[0][3] - m_transform[0][0],
		m_transform[1][3] - m_transform[1][0],
		m_transform[2][3] - m_transform[2][0],
		m_transform[3][3] - m_transform[3][0]);

	// left side
	m_planes[1] = vec4(
		m_transform[0][3] + m_transform[0][0],
		m_transform[1][3] + m_transform[1][0],
		m_transform[2][3] + m_transform[2][0],
		m_transform[3][3] + m_transform[3][0]);

	// top 
	m_planes[2] = vec4(
		m_transform[0][3] - m_transform[0][1],
		m_transform[1][3] - m_transform[1][1],
		m_transform[2][3] - m_transform[2][1],
		m_transform[3][3] - m_transform[3][1]);

	// bottom
	m_planes[3] = vec4(
		m_transform[0][3] + m_transform[0][1],
		m_transform[1][3] + m_transform[1][1],
		m_transform[2][3] + m_transform[2][1],
		m_transform[3][3] + m_transform[3][1]);

	// far 
	m_planes[4] = vec4(
		m_transform[0][3] - m_transform[0][2],
		m_transform[1][3] - m_transform[1][2],
		m_transform[2][3] - m_transform[2][2],
		m_transform[3][3] - m_transform[3][2]);

	// near 
	m_planes[5] = vec4(
		m_transform[0][3] + m_transform[0][2],
		m_transform[1][3] + m_transform[1][2],
		m_transform[2][3] + m_transform[2][2],
		m_transform[3][3] + m_transform[3][2]);

	// plane normalisation, based on length of normal 
	for (int i = 0; i < 6; i++)
	{
		float d = glm::length(vec3(m_planes[i]));
		m_planes[i] /= d;
	}
}