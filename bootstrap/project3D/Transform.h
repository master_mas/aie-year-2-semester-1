#pragma once
#include "Component.h"

class Transform : public Component
{
public:
	Transform();
	~Transform();

	glm::vec3 getPosition();
	void setPosition(glm::vec3 position);
	void rotate(glm::vec3 axis, float angle);

	void translate(glm::vec3 pos);

	glm::mat4* getTransform();

private:
	glm::mat4 transform;

	// Inherited via Component
	virtual void update(float deltaTime) override;
};

