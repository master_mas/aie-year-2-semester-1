#include "Texture2D.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>
//#include <stb_image.h>
#include <iostream>

Texture2D::Texture2D()
{
}

Texture2D::~Texture2D()
{
}

int Texture2D::loadTexture(const char* fileName, const char* type)
{
	m_path = std::string(fileName);
	m_type = std::string(type);

	glGenTextures(1, &m_texture);

	std::cout << fileName << std::endl;

	m_data = stbi_load(fileName, &m_width, &m_height, &m_fileType, STBI_default);

	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, m_data);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	stbi_image_free(m_data);

	return m_texture;
}

unsigned int Texture2D::getTexture()
{
	return m_texture;
}

std::string& Texture2D::getPath()
{
	return m_path;
}

std::string & Texture2D::getType()
{
	return m_type;
}
