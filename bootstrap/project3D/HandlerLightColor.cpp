#include "HandlerLightColor.h"
#include <vector>
#include <string>
#include <iterator>
#include <sstream>

HandlerLightColor::HandlerLightColor(glm::vec3 * lightColor) : NetworkDataHandler(3)
{
	this->lightColor = lightColor;
}

HandlerLightColor::~HandlerLightColor()
{
}

void HandlerLightColor::handleData(std::string data, Address * address)
{
	std::istringstream iss(data);
	std::vector<std::string> tokens{ std::istream_iterator<std::string>{iss},
		std::istream_iterator<std::string>{} };

	if (tokens.size() == 3)
	{
		float r = stof(tokens[0]);
		float g = stof(tokens[1]);
		float b = stof(tokens[2]);

		lightColor->r = r;
		lightColor->g = g;
		lightColor->b = b;
	}
}
