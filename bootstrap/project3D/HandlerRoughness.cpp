#include "HandlerRoughness.h"

HandlerRoughness::HandlerRoughness(float* roughness) : NetworkDataHandler(2)
{
	this->roughness = roughness;
}

HandlerRoughness::~HandlerRoughness()
{
}

void HandlerRoughness::handleData(std::string data, Address * address)
{
	float amount = stof(data);
	if (amount > 1)
	{
		amount = 1;
	}
	else if(amount < 0)
	{
		amount = 0;
	}

	*roughness = amount;
}
