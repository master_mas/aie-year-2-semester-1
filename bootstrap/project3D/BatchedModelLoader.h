#pragma once

#include <vector>
#include <memory>
#include "Model.h"

class BatchedModelLoader
{
public:
	BatchedModelLoader(char* address, unsigned int totalBatch);
	~BatchedModelLoader();

	std::vector<Model*>& getModels();

	const aiScene* loadModelData(char* address);

	void loadMaterialTextures(aiMaterial * mat, aiTextureType m_type, std::string typeName);

private:
	Assimp::Importer importer;
	std::vector<Model*> models;
	std::shared_ptr<std::vector<Texture2D*>> textures;
	std::string directory;

	void nodeExtractor(aiNode* node, const aiScene* scene);
	
};

