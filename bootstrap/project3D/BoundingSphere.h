#pragma once
#include <vector>
#include <glm\glm.hpp>
#include "Component.h"
class BoundingSphere : public Component
{
public:
	//Structors
	BoundingSphere();
	~BoundingSphere();

	//Functions
	void fit(const std::vector<glm::vec3>& points);
	void set(float radius, glm::vec3 centre);

	//Public Variables
	glm::vec3 m_centre;
	float m_radius;

	// Inherited via Component
	virtual void update(float deltaTime) override;
};

