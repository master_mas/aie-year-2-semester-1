#include "PostProcessing.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>



PostProcessing::PostProcessing()
{
	glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo); 

	glGenTextures(1, &m_fboTexture); 
	glBindTexture(GL_TEXTURE_2D, m_fboTexture); 
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, 1280, 720); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); 
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_fboTexture, 0); 

	glGenRenderbuffers(1, &m_fboDepth); glBindRenderbuffer(GL_RENDERBUFFER, m_fboDepth); 
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 1280, 720); 
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_fboDepth); 

	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 }; 
	glDrawBuffers(1, drawBuffers); 
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glm::vec2 halfTexel = 1.0f / glm::vec2(1280, 720) * 0.5f;
	
	float vertexData[] = { -1, -1, 0, 1, halfTexel.x, halfTexel.y, 
		1, 1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y, 
		-1, 1, 0, 1, halfTexel.x, 1 - halfTexel.y, 
		-1, -1, 0, 1, halfTexel.x, halfTexel.y, 
		1, -1, 0, 1, 1 - halfTexel.x, halfTexel.y, 
		1, 1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y, 
	};

	glGenVertexArrays(1, &m_vao); 
	glBindVertexArray(m_vao); 
	
	glGenBuffers(1, &m_vbo); 
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 6, vertexData, GL_STATIC_DRAW); 
	
	glEnableVertexAttribArray(0); 
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0); 
	
	glEnableVertexAttribArray(1); 
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 6, ((char*)0) + 16);
	
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


PostProcessing::~PostProcessing()
{
}

void PostProcessing::draw(Shader * shader, Camera * camera)
{
	glUseProgram(shader->getProgramID()); 

	glActiveTexture(GL_TEXTURE0); 
	glBindTexture(GL_TEXTURE_2D, m_fboTexture);
	glUniform1i(glGetUniformLocation(shader->getProgramID(), "target"), 0);

	glBindVertexArray(m_vao); 
	glDrawArrays(GL_TRIANGLES, 0, 6);

}

