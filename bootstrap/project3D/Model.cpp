#include "Model.h"
#include "Camera.h"

Model::Model(Model * takeIn)
{
	//this = takeIn;
}

Model::Model(const aiScene * scene, std::shared_ptr<std::vector<Texture2D*>> textures)
{
	m_textures_loaded = textures;
	this->processNode(scene->mRootNode, scene);
}

Model::~Model()
{
}

void Model::draw(Camera* camera)
{
	for (unsigned int i = 0; i < this->m_meshes.size(); i++)
	{
		this->m_meshes[i].draw(shader, camera, *(transform->getTransform()));
	}
}

void Model::setLightDirection(glm::vec3 direction)
{
	for (unsigned int i = 0; i < m_meshes.size(); ++i)
	{
		m_meshes[i].setLightDirection(direction);
	}
}

void Model::setLightColor(glm::vec3 color)
{
	for (unsigned int i = 0; i < m_meshes.size(); ++i)
	{
		m_meshes[i].setLightColor(color);
	}
}

void Model::setSpecularPower(float power)
{
	for (unsigned int i = 0; i < m_meshes.size(); ++i)
	{
		m_meshes[i].setSpecularPower(power);
	}
}

void Model::setRoughness(float m_roughness)
{
	for (unsigned int i = 0; i < m_meshes.size(); ++i)
	{
		m_meshes[i].setRoughness(m_roughness);
	}
}

std::vector<glm::vec3> Model::getVBOPoints()
{
	std::vector<glm::vec3> points;
	
	for (unsigned int i = 0; i < m_meshes.size(); ++i)
	{
		for (unsigned int ii = 0; ii < m_meshes[i].m_vertices.size(); ++ii)
		{
			glm::vec4 temp = m_meshes[i].m_vertices[ii].position;
			points.push_back(glm::vec3(temp.x, temp.y, temp.z));
		}
	}

	return points;
}

void Model::processNode(aiNode * node, const aiScene * scene)
{
	for (GLuint i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		this->m_meshes.push_back(this->processMesh(mesh, scene));
	}
	// Then do the same for each of its children
	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		this->processNode(node->mChildren[i], scene);
	}
}

Mesh Model::processMesh(aiMesh * mesh, const aiScene * scene)
{
	std::vector<Structure::Vertex> m_vertices;
	std::vector<GLuint> m_indices;

	for (GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Structure::Vertex vertex;
		// Process vertex positions, normals and texture coordinates

		glm::vec4 vector4;
		vector4.x = mesh->mVertices[i].x;
		vector4.y = mesh->mVertices[i].y;
		vector4.z = mesh->mVertices[i].z;
		vector4.w = 1;
		vertex.position = vector4;
		
		glm::vec3 vector3;
		vector3.x = mesh->mNormals[i].x;
		vector3.y = mesh->mNormals[i].y;
		vector3.z = mesh->mNormals[i].z;
		vertex.normal = vector3;

		if (mesh->mTextureCoords[0])
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.texCoord = vec;
		}
		else
			vertex.texCoord = glm::vec2(0.0f, 0.0f);

		if (mesh->mTangents)
		{
			glm::vec4 vector41;
			vector41.x = mesh->mTangents[i].x;
			vector41.y = mesh->mTangents[i].y;
			vector41.z = mesh->mTangents[i].z;
			vertex.tangants = vector41;
		}
		else
		{
			vertex.tangants = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
		}

		m_vertices.push_back(vertex);
	}
	
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (GLuint j = 0; j < face.mNumIndices; j++)
			m_indices.push_back(face.mIndices[j]);
	}

	return Mesh(m_vertices, m_indices, m_textures_loaded);
}