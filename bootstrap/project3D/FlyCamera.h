#pragma once
#include "Camera.h"

//Extends Camera
class FlyCamera :
	public Camera
{
public:
	//Structors
	FlyCamera();
	~FlyCamera();

	//Public Functions
	void update(float deltaTime, GLFWwindow* window);
	void setSpeed(float speed);
	void rotateCameraUsingKeys(float delta, GLFWwindow* window);
	void moveCameraUsingKeys(float delta, GLFWwindow* window);

private:
	//Private Variables
	float m_rotateSpeed = .01f;
	float m_moveSpeed = 5;
	vec3 m_up = vec3(0, 1, 0);
};

