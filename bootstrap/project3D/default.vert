#version 410

layout(location=0) in vec4 vertexPosition;
layout(location=1) in vec4 tangent;
layout(location=2) in vec3 normal;
layout(location=3) in vec2 texCoord;

out vec2 vTexCoord;
out vec3 vNormal;
out vec4 vPosition;
out vec3 vTangent; 
out vec3 vBiTangent;

uniform mat4 projectionViewMatrix;
uniform mat4 modelMatrix;

void main() 
{
	vTexCoord = texCoord;
	vNormal = normal;
	vPosition = vertexPosition;
	vTangent = tangent.xyz; 
	vBiTangent = cross(vNormal, vTangent);
	
	gl_Position = projectionViewMatrix * modelMatrix * vertexPosition;
}