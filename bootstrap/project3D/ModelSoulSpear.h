#pragma once
#include "Model.h"
class ModelSoulSpear :
	public Model
{
public:
	ModelSoulSpear(const aiScene * scene, std::shared_ptr<std::vector<Texture2D*>> textures);
	~ModelSoulSpear();

	void update(float deltaTime) override;
};

