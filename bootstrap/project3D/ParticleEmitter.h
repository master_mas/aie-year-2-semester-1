#pragma once
#include "Entity.h"
#include "Structure.h"
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <gl_core_4_4.h>

#include "Shader.h"
#include "Camera.h"

class ParticleEmitter : public Entity
{
public:
	//Structors
	ParticleEmitter();
	ParticleEmitter(glm::vec3 position);
	~ParticleEmitter();

	//Public Functions
	void initalise(unsigned int a_maxParticles, unsigned int a_emitRate, float a_lifetimeMin, 
		float a_lifetimeMax, float a_velocityMin, float a_velocityMax, float a_startSize, float a_endSize, 
		const glm::vec4& a_startColour, const glm::vec4& a_endColour);

	void emit(float deltatime);
	void update(float deltaTime);
	void draw(Camera* camera);

protected:
	//Protected Variables
	Structure::Particle* m_particles;
	unsigned int m_firstDead;
	unsigned int m_maxParticles;

	unsigned int m_vao, m_vbo, m_ibo;
	Structure::ParticleVertex* m_vertexData;

private:
	//Private Variables
	glm::vec3 m_position;

	float m_emitTimer = 0;
	float m_emitRate = 0;
	
	float m_lifespanMin = 0;
	float m_lifespanMax = 0;
	
	float m_velocityMin = 0;
	float m_velocityMax = 0;
	
	float m_startSize = 0;
	float m_endSize = 0; 
	
	glm::vec4 m_startColour = glm::vec4(1, 1, 1, 1);
	glm::vec4 m_endColour = glm::vec4(1, 1, 1, 1);
};

