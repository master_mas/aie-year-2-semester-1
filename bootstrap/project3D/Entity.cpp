#include "Entity.h"

Entity::Entity()
{
	transform = new Transform();
	addComponent(transform);
}

Entity::~Entity()
{
	for (unsigned int i = 0; i < components.size(); ++i)
	{
		delete components[i];
	}

	transform = nullptr;
}

void Entity::draw(Camera * camera)
{

}

void Entity::update(float deltatime)
{
	for (unsigned int i = 0; i < components.size(); ++i)
	{
		components[i];
	}
}

void Entity::addComponent(Component* component)
{
	components.push_back(component);
}

void Entity::setShader(Shader * shader)
{
	this->shader = shader;
}

void Entity::setShaderLoader(ShaderLoader * shadlerLoader)
{
	this->shaderLoader = shaderLoader;
}

void Entity::setCamera(Camera * camera)
{
	this->camera = camera;
}

Transform * Entity::getTransform()
{
	return transform;
}
