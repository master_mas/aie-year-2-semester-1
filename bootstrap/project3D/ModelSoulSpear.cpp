#include "ModelSoulSpear.h"


ModelSoulSpear::ModelSoulSpear(const aiScene * scene, std::shared_ptr<std::vector<Texture2D*>> textures) : Model(scene, textures)
{
}

ModelSoulSpear::~ModelSoulSpear()
{
}

void ModelSoulSpear::update(float deltaTime)
{
	transform->rotate(glm::vec3(0, 1, 0), 5 * deltaTime);
}
