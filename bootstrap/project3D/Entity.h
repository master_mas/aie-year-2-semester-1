#pragma once

#include "Shader.h"
#include "Camera.h"
#include "Component.h"
#include "Transform.h"
#include "ShaderLoader.h"

#include <vector>

class Entity
{
public:
	Entity();
	~Entity();

	virtual void draw(Camera* camera);
	virtual void update(float deltatime);

	void addComponent(Component* component);
	template<typename T> bool hasComponent();
	template<typename T> Component* getComponent();
	template<typename T> bool removeComponent();

	void setShader(Shader* shader);
	void setShaderLoader(ShaderLoader* shadlerLoader);

	void setCamera(Camera* camera);

	Transform* getTransform();

protected:
	Shader* shader = nullptr;
	unsigned int vbo;
	unsigned int vao;
	unsigned int ibo;
	Transform* transform =  nullptr;
	ShaderLoader* shaderLoader;

	Camera* camera;
private:
	std::vector<Component*> components;
};

template<typename T>
bool Entity::removeComponent()
{
	for (int i = 0; i < components.size(); ++i)
	{
		if (typeid(components[i]) == typeid(T))
		{
			components.erase(components.begin() + i);
			return true;
		}
	}

	return false;
}

template<typename T>
bool Entity::hasComponent()
{
	for (unsigned int i = 0; i < components.size(); ++i)
	{
		if (dynamic_cast<T*>(components[i]) != 0)
		{
			return true;
		}
	}

	return false;
}

template<typename T>
Component * Entity::getComponent()
{
	for (unsigned int i = 0; i < components.size(); ++i)
	{
		if (dynamic_cast<T*>(components[i]) != 0)
		{
			return components[i];
		}
	}

	return nullptr;
}
