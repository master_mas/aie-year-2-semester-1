#include "Application3D.h"
#include "Gizmos.h"
#include "Input.h"
#include "BatchedModelLoader.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <string>
#include "HandlerRoughness.h"
#include "HandlerLightColor.h"
#include "HandlerLightDirection.h"
#include "HandlerSpecular.h"


Application3D::Application3D() { }

Application3D::~Application3D() { }

//Init for Project
bool Application3D::startup() 
{
	//Create AIE Bootstrap Gizmos
	aie::Gizmos::create(10000, 10000, 10000, 10000);

	//Create Camera and Set positions
	m_camera = new FlyCamera();
	m_camera->setPerspective(glm::pi<float>() * 0.25f, (float)getWindowWidth() / (float)getWindowHeight(), 0.1f, 1000.f);
	m_camera->setLookAt(glm::vec3(8, 3, -5), glm::vec3(0), glm::vec3(0, 1, 0));
	m_camera->setPosition(glm::vec3(10, 3, 0));

	shaderLoader = new ShaderLoader();

	//Create Models
	BatchedModelLoader loader = BatchedModelLoader("models/soulspear/soulspear.obj", 25);
	std::vector<Model*> models = loader.getModels();
	glm::vec3 pos = glm::vec3(10, 0, 10);
	for (unsigned int i = 0; i < models.size(); ++i)
	{
		models[i]->getTransform()->setPosition(pos);
		models[i]->setShader(shaderLoader->getShader(ShaderLoader::SHADER_TEXTURE_WITH_LIGHTING));
		models[i]->setCamera(m_camera);
		BoundingSphere* sphere = new BoundingSphere();
		sphere->fit(models[i]->getVBOPoints());
		models[i]->addComponent(sphere);
		addEntity(models[i]);

		pos.x -= 5;
		if (pos.x < -10)
		{
			pos.z -= 5;
			pos.x = 10;
		}
	}

	//Create Particle Emitters
	pos = glm::vec3(8, 0, 8);
	for (unsigned int i = 0; i < 16; ++i)
	{
		ParticleEmitter *emitter = new ParticleEmitter(pos);
		emitter->initalise(
			int(1000 / glm::length(pos)), // Max Particles
			int(250 / glm::length(pos)), // Emit Rate
			0.1f, // Lifetime Min
			1.0f, // Lifetime Max
			1.0f, // Velocity Min
			5.0f, // Velocity Max
			1.0f, // Start Size
			0.1f, // End Size
			glm::vec4(glm::normalize(pos), 1), // Color Start
			glm::vec4(0.0f, 1.0f, 0.0f, 1.0f)); // Color End

		emitter->getTransform()->setPosition(pos);
		emitter->setShader(shaderLoader->getShader(ShaderLoader::SHADER_PARTICLE));
		emitter->setCamera(m_camera);
		BoundingSphere* sphere = new BoundingSphere();
		sphere->set(1.0f, glm::vec3(0, 0, 0));
		emitter->addComponent(sphere);
		addEntity(emitter);

		pos.x -= 5;
		if (pos.x < -8)
		{
			pos.z -= 5;
			pos.x = 8;
		}
	}

	//Create Post Processing
	m_postProcessing = new PostProcessing();
	/*networkClient = new NetworkServer();
	networkClient->registerDataHandler(new HandlerRoughness(&m_roughness));
	networkClient->registerDataHandler(new HandlerLightColor(&m_lightColor));
	networkClient->registerDataHandler(new HandlerLightDirection(m_lightDirection));
	networkClient->registerDataHandler(new HandlerSpecular(&m_specularPower));*/

	return true;
}

//Shutdown Project and Cleanup
void Application3D::shutdown() 
{
	for (unsigned int i = 0; i < entities.size(); ++i)
	{
		delete entities[i];
	}

	delete m_camera;
	delete m_postProcessing;
	delete shaderLoader;
}

//Project Update Loop
unsigned int clientID = 0;
void Application3D::update(float deltaTime) 
{
	float time = getTime();

	//if (networkClient->acceptNewClient(clientID))
	//{
	//	clientID++;
	//	std::cout << "Client has been connected to the server\n";
	//}
	//networkClient->update();

	//Update Camera
	m_camera->update(deltaTime, m_window);

	for (unsigned int i = 0; i < entities.size(); ++i)
	{
		entities[i]->update(deltaTime);

		if (dynamic_cast<Model*>(entities[i]) != 0)
		{
			((Model*)entities[i])->setLightDirection(glm::vec3(m_lightDirection[0], m_lightDirection[1], m_lightDirection[2]));
			((Model*)entities[i])->setLightColor(m_lightColor);
			((Model*)entities[i])->setRoughness(m_roughness);
			((Model*)entities[i])->setSpecularPower(m_specularPower);
		}
	}

	//Clear all current gizmos
	aie::Gizmos::clear();
}

void Application3D::draw() 
{
	//Create before
	glBindFramebuffer(GL_FRAMEBUFFER, m_postProcessing->m_fbo);
	glViewport(0, 0, 1280, 720);

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	m_camera->getFrustumPlanes(m_planes);

	m_totalRender = 0;

	for (unsigned int i = 0; i < entities.size(); ++i)
	{
		bool renderAllow = true;
		if (entities[i]->hasComponent<BoundingSphere>())
		{
			BoundingSphere* bs = (BoundingSphere*)(entities[i]->getComponent<BoundingSphere>());
			for (int ii = 0; ii < 6; ii++)
			{
				float d = glm::dot(vec3(m_planes[ii]), bs->m_centre + entities[i]->getTransform()->getPosition()) + m_planes[ii].w;
				if (d < -bs->m_radius)
				{
					renderAllow = false;
					break;
				}
			}
		}

		if (renderAllow)
		{
			entities[i]->draw(m_camera);
			m_totalRender++;
			//aie::Gizmos::addSphere(m_models[i].getBoundingSphere().m_centre + m_models[i].getPos(), m_models[i].getBoundingSphere().m_radius, 8, 8, vec4(1, 0, 1, 1));
		}
	}

	aie::Gizmos::draw(m_camera->getProjectionView());
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); 
	//glViewport(0, 0, 1280, 720);

	//clearScreen();
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	m_postProcessing->draw(shaderLoader->getShader(ShaderLoader::SHADER_POST_PROCESS), m_camera);

	glClearColor(m_clearColour.r, m_clearColour.g, m_clearColour.b, 1);

	ImGui::Begin("My Options");
	ImGui::ColorEdit3("Clear Colour", glm::value_ptr(m_clearColour));
	ImGui::InputFloat3("Light Direction", m_lightDirection);
	ImGui::ColorEdit3("Light Colour", glm::value_ptr(m_lightColor));
	ImGui::DragFloat("Specular Power", &m_specularPower, 1.0f, 0.0f, 1.0f, "%.3f", 0.01f);
	ImGui::DragFloat("Light Roughness", &m_roughness, 1.0f, 0.0f, 1.0f, "%.3f", 0.01f);
	ImGui::InputInt("Render Calls", &m_totalRender);
	ImGui::End();
}

void Application3D::addEntity(Entity * entity)
{
	entities.push_back(entity);
	entity->setShaderLoader(shaderLoader);
}
