#pragma once

//#define STB_IMAGE_IMPLEMENTATION 
#include <stb_image.h>
#include <string>

class Texture2D
{
public:
	//Structors
	Texture2D();
	~Texture2D();

	//Public Functions
	int loadTexture(const char* fullPath, const char* type);
	unsigned int getTexture();

	std::string& getPath();
	std::string& getType();
private:
	//Private Variables
	int m_width = 0;
	int m_height = 0;

	int m_fileType = 0;

	std::string m_path;
	std::string m_type;

	unsigned char* m_data = nullptr;
	unsigned int m_texture = 0;
};