#include "ShaderLoader.h"

ShaderLoader::ShaderLoader()
{
	add("../project3D/default.vert", "../project3D/default-physically-lighting.frag");
	add("../project3D/Particle.vert", "../project3D/Particle.frag");
	add("../project3D/PostProcess.vert", "../project3D/PostProcess.frag");
}

ShaderLoader::~ShaderLoader()
{
	for (unsigned int i = 0; i < shaders.size(); ++i)
	{
		delete shaders[i];
	}
}

Shader* ShaderLoader::getShader(unsigned int shader)
{
	if (shader < shaders.size())
	{
		return shaders[shader];
	}

	return nullptr;
}

void ShaderLoader::add(const char * vertex, const char * fragment)
{
	shaders.push_back(new Shader());
	shaders[shaders.size() - 1]->loadShader(vertex, fragment);
}
