#include "ParticleEmitter.h"

ParticleEmitter::ParticleEmitter()
{
}

ParticleEmitter::ParticleEmitter(glm::vec3 position)
	: m_particles(nullptr), m_firstDead(0), m_maxParticles(0), m_vao(0), m_vbo(0), 
	m_ibo(0), m_vertexData(nullptr) 
{
	m_position = glm::vec3(position);
}


ParticleEmitter::~ParticleEmitter()
{
	//delete[] m_particles; 
	//delete[] m_vertexData; 
	glDeleteVertexArrays(1, &m_vao); 
	glDeleteBuffers(1, &m_vbo); 
	glDeleteBuffers(1, &m_ibo);
}

void ParticleEmitter::initalise(unsigned int maxParticles, unsigned int emitRate, float lifetimeMin, 
	float lifetimeMax, float velocityMin, float velocityMax, float startSize, float endSize, 
	const glm::vec4 & startColour, const glm::vec4 & endColour)
{
	m_emitTimer = 0; 
	m_emitRate = 1.0f / emitRate;

	m_startColour = startColour;
	m_endColour = endColour;
	m_startSize = startSize;
	m_endSize = endSize;
	m_velocityMin = velocityMin;
	m_velocityMax = velocityMax;
	m_lifespanMin = lifetimeMin;
	m_lifespanMax = lifetimeMax;
	m_maxParticles = maxParticles;

	m_particles = new Structure::Particle[m_maxParticles];
	m_firstDead = 0;

	m_vertexData = new Structure::ParticleVertex[m_maxParticles * 4];

	unsigned int* indexData = new unsigned int[m_maxParticles * 6];
	for (unsigned int i = 0; i < m_maxParticles; ++i)
	{
		indexData[i * 6 + 0] = i * 4 + 0;
		indexData[i * 6 + 1] = i * 4 + 1;
		indexData[i * 6 + 2] = i * 4 + 2;

		indexData[i * 6 + 3] = i * 4 + 0;
		indexData[i * 6 + 4] = i * 4 + 2;
		indexData[i * 6 + 5] = i * 4 + 3;
	}

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * 4 * sizeof(Structure::ParticleVertex), m_vertexData, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxParticles * 6 * sizeof(GLuint), indexData, GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Structure::ParticleVertex), 0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Structure::ParticleVertex), (void*)(sizeof(glm::vec4)));

	glBindVertexArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] indexData;
}

void ParticleEmitter::emit(float deltaTime)
{
	if (m_firstDead >= m_maxParticles)
	{
		return; // resurrect the first dead particle 
	}
	Structure::Particle& particle = m_particles[m_firstDead++];
	
	// assign its starting position 
	particle.position = m_position;

	// randomise its lifespan	
	particle.lifeTime = 0; 
	particle.lifeSpan = (rand() / (float)RAND_MAX) * (m_lifespanMax - m_lifespanMin) + m_lifespanMin; 
	
	// set starting size and colour 
	particle.color = m_startColour;
	particle.size = m_startSize; // randomise velocity direction and strength 
	float velocity = (rand() / (float)RAND_MAX) * (m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity = glm::normalize(particle.velocity) * velocity;
}

void ParticleEmitter::update(float deltaTime)
{
	m_emitTimer += deltaTime;
	while (m_emitTimer > m_emitRate) 
	{ 
		emit(deltaTime);
		m_emitTimer -= m_emitRate; 
	}

	unsigned int quad = 0;

	for (unsigned int i = 0; i < m_firstDead; i++) {
		Structure::Particle* particle = &m_particles[i]; 
		particle->lifeTime += deltaTime; 
		if (particle->lifeTime >= particle->lifeSpan)
		{ 
			// swap last alive with this one 
			*particle = m_particles[m_firstDead - 1]; 
			m_firstDead--; 
		} 
		else 
		{ 
			// move particle 
			particle->position += particle->velocity * deltaTime; 
			
			// size particle 
			particle->size = glm::mix(m_startSize, m_endSize, particle->lifeTime / particle->lifeSpan);
			
			// colour particle 
			particle->color = glm::mix(m_startColour, m_endColour, particle->lifeTime / particle->lifeSpan);

			// make a quad the correct size and colour 
			float halfSize = particle->size * 0.5f;
			m_vertexData[quad * 4 + 0].position = glm::vec4(halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 0].color = particle->color;
			m_vertexData[quad * 4 + 1].position = glm::vec4(-halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 1].color = particle->color; 
			m_vertexData[quad * 4 + 2].position = glm::vec4(-halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 2].color = particle->color; 
			m_vertexData[quad * 4 + 3].position = glm::vec4(halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 3].color = particle->color; // create billboard transform 
			glm::vec3 zAxis = glm::normalize(glm::vec3(camera->getWorldTransform()[3]) - particle->position); 
			glm::vec3 xAxis = glm::cross(glm::vec3(camera->getWorldTransform()[1]), zAxis);
			glm::vec3 yAxis = glm::cross(zAxis, xAxis); 
			
			glm::mat4 billboard(glm::vec4(xAxis, 0), glm::vec4(yAxis, 0), glm::vec4(zAxis, 0), glm::vec4(0,0,0,1)); 
			m_vertexData[quad * 4 + 0].position = billboard * m_vertexData[quad * 4 + 0].position + glm::vec4(particle->position,0);
			m_vertexData[quad * 4 + 1].position = billboard * m_vertexData[quad * 4 + 1].position + glm::vec4(particle->position,0); 
			m_vertexData[quad * 4 + 2].position = billboard * m_vertexData[quad * 4 + 2].position + glm::vec4(particle->position,0);
			m_vertexData[quad * 4 + 3].position = billboard * m_vertexData[quad * 4 + 3].position + glm::vec4(particle->position,0); ++quad; 
		} 
	}
}

void ParticleEmitter::draw(Camera* camera)
{
	glUseProgram(shader->getProgramID());
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgramID(), "projectionView"), 1, GL_FALSE, &camera->getProjectionView()[0][0]);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_firstDead * 4 * sizeof(Structure::ParticleVertex), m_vertexData);
	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, m_firstDead * 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}
