#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>
#include <fstream>
#include <iostream>

class Shader
{
public:
	//Structors
	Shader();
	~Shader();

	//Public Functions
	void loadShader(const char* vertPath, const char* fragPath);
	unsigned int getProgramID();

private:
	//Private Variables
	unsigned int m_programID = 0;

	//Private Functions
	std::string readFile(const char* filePath);
};

