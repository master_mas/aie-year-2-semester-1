#include "Mesh.h"
#include <iostream>

Mesh::Mesh(std::vector<Structure::Vertex> m_vertices, std::vector<GLuint> m_indices, std::shared_ptr<std::vector<Texture2D*>> textures)
{
	this->m_vertices = m_vertices;
	this->m_indices = m_indices;
	this->m_textures = textures;

	this->setupMesh();
}

Mesh::~Mesh()
{
}

void Mesh::setupMesh()
{
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

	glBufferData(GL_ARRAY_BUFFER, this->m_vertices.size() * sizeof(Structure::Vertex), &this->m_vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->m_indices.size() * sizeof(GLuint),
		&this->m_indices[0], GL_STATIC_DRAW);

	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), (GLvoid*)0);
	//Vertex Tangants
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), (void*)(sizeof(glm::vec4)));
	// Vertex Normals
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), (void*)(sizeof(glm::vec4) * 2));
	// Vertex Texture Coords
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), (void*)(sizeof(glm::vec4) * 2 + sizeof(glm::vec3)));

	glBindVertexArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, 0); 
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Mesh::draw(Shader* shader, Camera* camera, glm::mat4 & position)
{
	glUseProgram(shader->getProgramID());

	glUniformMatrix4fv(glGetUniformLocation(shader->getProgramID(), "projectionViewMatrix"), 1, GL_FALSE, &(camera->getProjectionView()[0][0]));
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgramID(), "modelMatrix"), 1, GL_FALSE, &(position[0][0]));

	GLuint diffuseNr = 1;
	GLuint specularNr = 1;
	GLuint normalNr = 1;
	for (GLuint i = 0; i < m_textures->size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0 + i);

		glUniform1i(glGetUniformLocation(shader->getProgramID(), ("material." + m_textures->at(i)->getType() + "1").c_str()), i);

		glBindTexture(GL_TEXTURE_2D, m_textures->at(i)->getTexture());
	}

	glUniform3f(glGetUniformLocation(shader->getProgramID(), "lightDirection"), m_lightDirection.x, m_lightDirection.y, m_lightDirection.z);
	glUniform3f(glGetUniformLocation(shader->getProgramID(), "lightColor"), m_lightColor.x, m_lightColor.y, m_lightColor.z);
	glUniform3f(glGetUniformLocation(shader->getProgramID(), "cameraPos"), camera->getPosition().x, camera->getPosition().y, camera->getPosition().z);
	glUniform1f(glGetUniformLocation(shader->getProgramID(), "specPow"), m_specularPower);
	glUniform1f(glGetUniformLocation(shader->getProgramID(), "roughness"), m_roughness);

	glActiveTexture(GL_TEXTURE0);

	// Draw mesh
	glBindVertexArray(this->VAO);
	glDrawElements(GL_TRIANGLES, this->m_indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

//void Mesh::generateGrid(unsigned int rows, unsigned int cols, int offsetX, int offsetY, int offsetZ)
//{
//	m_cols = cols;
//	m_rows = rows;
//	Structure::VertexOld* aoVertices = new Structure::VertexOld[rows * cols];
//	for (unsigned int r = 0; r < rows; ++r)
//	{
//		for (unsigned int c = 0; c < cols; ++c)
//		{
//			aoVertices[r * cols + c].position = glm::vec4((float)c + offsetX, 0 + offsetY, (float)r + offsetZ, 1);
//			aoVertices[r * cols + c].texCoord = glm::vec2(r, c);
//			glm::vec3 colour = glm::vec3(-sinf((c / (float)(cols - 1)) * -(r / (float)(rows - 1))), -sinf((c / (float)(cols - 1)) * (r / (float)(rows - 1))), sinf((c / (float)(cols - 1)) * (r / (float)(rows - 1))));
//			aoVertices[r * cols + c].colour = glm::vec4(colour, 1);
//		}
//	}
//
//	unsigned int* auiIndices = new unsigned int[(rows - 1) * (cols - 1) * 6];
//	unsigned int index = 0;
//	for (unsigned int r = 0; r < (rows - 1); ++r)
//	{
//		for (unsigned int c = 0; c < (cols - 1); ++c)
//		{
//			// triangle 1
//			auiIndices[index++] = r * cols + c;
//			auiIndices[index++] = (r + 1) * cols + c;
//			auiIndices[index++] = (r + 1) * cols + (c + 1);
//			// triangle 2 
//			auiIndices[index++] = r * cols + c;
//			auiIndices[index++] = (r + 1) * cols + (c + 1);
//			auiIndices[index++] = r * cols + (c + 1);
//		}
//	}
//
//	glGenBuffers(1, &m_VBO);
//	glGenBuffers(1, &m_IBO);
//
//	glGenVertexArrays(1, &m_VAO);
//	glBindVertexArray(m_VAO);
//
//	//VBO
//	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
//	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(Structure::VertexOld), aoVertices, GL_STATIC_DRAW);
//
//	glEnableVertexAttribArray(0);
//	glEnableVertexAttribArray(1);
//	glEnableVertexAttribArray(2);
//	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Structure::VertexOld), 0);
//	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Structure::VertexOld), (void*)(sizeof(glm::vec4)));
//	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Structure::VertexOld), (void*)(sizeof(glm::vec4) * 2));
//
//	//IBO
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(GLuint), auiIndices, GL_STATIC_DRAW);
//
//	glBindVertexArray(0);
//
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//
//	delete[] aoVertices;
//	delete[] auiIndices;
//}

void Mesh::setLightDirection(glm::vec3 direction)
{
	this->m_lightDirection = direction;
}

void Mesh::setLightColor(glm::vec3 color)
{
	this->m_lightColor = color;
}

void Mesh::setSpecularPower(float power)
{
	this->m_specularPower = power;
}

void Mesh::setRoughness(float m_roughness)
{
	this->m_roughness = m_roughness;
}
