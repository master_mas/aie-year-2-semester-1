#pragma once
#include "NetworkDataHandler.h"
class HandlerSpecular :
	public NetworkDataHandler
{
public:
	HandlerSpecular(float* specular);
	~HandlerSpecular();

	// Inherited via NetworkDataHandler
	virtual void handleData(std::string data, Address * address) override;
	
private:
	float * specular;
};

