#include "NetworkDataListener.h"

NetworkDataListener::NetworkDataListener(std::vector<NetworkDataHandler*>* networkHandlers, Socket* socket, std::vector<Address*>* currentlyConnected, char* topLevelAddressMeta)
{
	this->networkHandlers = networkHandlers;
	this->socket = socket;
	this->currentlyConnected = currentlyConnected;
	this->topLevelAddressMeta = topLevelAddressMeta;

	for (unsigned int i = 0; i < networkHandlers->size(); ++i)
	{
		referenceToHandlers.push_back(networkHandlers->at(i)->getDataType());
	}
}

NetworkDataListener::~NetworkDataListener()
{
}

void NetworkDataListener::notifyHandlerUpdate()
{
	for (unsigned int i = 0; i < networkHandlers->size(); i++)
	{
		if (i >= referenceToHandlers.size())
		{
			referenceToHandlers.push_back(networkHandlers->at(i)->getDataType());
		}
		else if (referenceToHandlers[i] != networkHandlers->at(i)->getDataType())
		{
			referenceToHandlers[i] = networkHandlers->at(i)->getDataType();
		}
	}
}

void NetworkDataListener::recieveData()
{
	for (unsigned int i = 0; i < currentlyConnected->size(); ++i)
	{
		int checkCounter = 0;
		while (checkPendingNetworkQueues(currentlyConnected->at(i))) 
		{
			checkCounter++;

			if (checkCounter >= NETWORK_RECIEVING_QUEUE_TIMEOUT)
			{
				break;
			}
		}
	}
}

void NetworkDataListener::setRunning(bool running)
{
	this->isRunning = running;
}

bool NetworkDataListener::checkPendingNetworkQueues(Address* listen)
{
	if (socket->IsOpen())
	{
		unsigned char buffer[256];
		int bytes_read = socket->Receive(*listen, buffer, BUFFER_SIZE);

		if (bytes_read > 0)
		{
			const char* packet_data = (const char*)buffer;
			for (unsigned int i = 0; i < 4; ++i)
			{
				if (packet_data[i] != topLevelAddressMeta[i])
				{
					//return true;
				}
			}

			char* networkCode = new char[2];
			for (unsigned int i = 4; i < 2; ++i)
			{
				networkCode[i - 4] = packet_data[i];
			}

			int code = atoi(networkCode);
			delete[] networkCode;
			for (unsigned int i = 0; i < referenceToHandlers.size(); i++)
			{
				if (referenceToHandlers[i] == code)
				{
					//networkHandlers->at(i)->handleData(buffer, listen);
					break;
				}
			}

			return true;
		}
	}

	return false;
}
