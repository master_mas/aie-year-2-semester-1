#include "Packet.h"

template<typename T>
Packet<T>::Packet(const T & data, short dataType)
{
	this->data = data;
}

template<typename T>
Packet<T>::~Packet()
{

}

template<typename T>
T & Packet<T>::getData()
{
	return data;
}

template<typename T>
const char * Packet<T>::serializeData(const char* topLevelMeta)
{
	return (const char*)data;
}
