#pragma once

template<typename T>
class Packet
{
public:
	Packet(const T& data, short dataType);
	~Packet();

	T& getData();
	const char* serializeData();
private:
	const T &data;
};

