#define PLATFORM_WINDOWS  1
#define PLATFORM_MAC      2
#define PLATFORM_UNIX     3

#if defined(_WIN32)
#define PLATFORM PLATFORM_WINDOWS
#elif defined(__APPLE__)
#define PLATFORM PLATFORM_MAC
#else
#define PLATFORM PLATFORM_UNIX
#endif

#if PLATFORM == PLATFORM_WINDOWS
#include <winsock2.h>
#elif PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#endif

#if PLATFORM == PLATFORM_WINDOWS
#pragma comment( lib, "wsock32.lib" )
#endif

#pragma once
#include "Packet.h"
#include "Address.h"
#include "NetworkDataHandler.h"
#include "INetworkBase.h"

#include <string>
#include <iostream>
#include <vector>

class INetworkBase
{
public:
	const static int PORT = 30000;
	const static int BUFFER_SIZE = 1024;

	virtual ~INetworkBase() {};
	virtual void update() = 0;

	void registerDataHandler(NetworkDataHandler* handler);
	virtual void sendData(const char* data, int messageType) = 0;

protected:
	bool isExit = false;
	char buffer[INetworkBase::BUFFER_SIZE];

	std::vector<NetworkDataHandler*> networkDataHandlersList;

	NetworkDataHandler* getDataHandler(int messageType);
};

