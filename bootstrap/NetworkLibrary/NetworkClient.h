#pragma once
#include "Address.h"
#include "INetworkBase.h"
#include <ws2tcpip.h>
#include <thread>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

class NetworkClient : INetworkBase
{
public:
	NetworkClient();
	~NetworkClient();

	virtual void update() override;
	int receivePackets();
private:
	int client;
	int iResult;
	struct sockaddr_in server_addr;

	// Inherited via INetworkBase
	void sendData(const char * data, int messageType) override;


	void getMessage();
	
};

