#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#pragma once

#include "Address.h"
#include "INetworkBase.h"

#include <ws2tcpip.h>
#include <map>

class NetworkServer : public INetworkBase
{
public:
	NetworkServer();
	~NetworkServer();
	bool acceptNewClient(unsigned int & id);
	virtual void update() override;

	int receiveData(unsigned int client_id);

	// Inherited via INetworkBase
	void sendData(const char * data, int messageType) override;

private:
	int client, server;
	int iResult;
	struct sockaddr_in server_addr;
	socklen_t size;

	std::vector<NetworkDataHandler*> handlers;
	std::map<unsigned int, int> sessions;
};

