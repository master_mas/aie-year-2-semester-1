#include "NetworkServer.h"
#include "NetworkHandlerList.h"
#include <string>

NetworkServer::NetworkServer()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSADATA WsaData;
	WSAStartup(MAKEWORD(2, 2), &WsaData) == NO_ERROR;
#else
	return true;
#endif

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;    // TCP connection!!!
	hints.ai_flags = AI_PASSIVE;

	iResult = getaddrinfo(NULL, "30000", &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		exit(1);
	}

	client = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	if (client == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		exit(1);
	}

	u_long iMode = 1;
	iResult = ioctlsocket(client, FIONBIO, &iMode);

	if (iResult == SOCKET_ERROR) {
		printf("ioctlsocket failed with error: %d\n", WSAGetLastError());
		closesocket(client);
		WSACleanup();
		exit(1);
	}

	iResult = bind(client, result->ai_addr, (int)result->ai_addrlen);

	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(client);
		WSACleanup();
		exit(1);
	}

	freeaddrinfo(result);

	iResult = listen(client, SOMAXCONN);

	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(client);
		WSACleanup();
		exit(1);
	}
}

NetworkServer::~NetworkServer()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSACleanup();
#endif
}

bool NetworkServer::acceptNewClient(unsigned int & id)
{
	// if client waiting, accept the connection and save the socket
	server = accept(client, NULL, NULL);

	if (server != INVALID_SOCKET)
	{
		//disable nagle on the client's socket
		char value = 1;
		setsockopt(server, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));

		// insert new client into session id table
		sessions.insert(std::pair<unsigned int, SOCKET>(id, server));

		return true;
	}

	return false;
}

void NetworkServer::sendData(const char * data, int messageType)
{
	std::string prefixMessage = "&";
	std::string code = std::to_string(messageType);
	std::string subfixMessage = "&";
	std::string construct = std::string(data);
	std::string endMessage = "*";

	prefixMessage.append(code);
	prefixMessage.append(subfixMessage);
	prefixMessage.append(construct);
	prefixMessage.append(endMessage);

	const char* buf = prefixMessage.c_str();
	send(server, buf, BUFFER_SIZE, 0);
}


void NetworkServer::update()
{
	// go through all clients
	std::map<unsigned int, int>::iterator iter;

	for (iter = sessions.begin(); iter != sessions.end(); iter++)
	{
		int data_length = receiveData(iter->first);

		if (data_length <= 0)
		{
			//no data recieved
			continue;
		}

		std::string messageType;
		std::string message;
		bool type = false;

		for (unsigned int i = 0; i < BUFFER_SIZE; ++i)
		{
			if ((buffer[i]) == '&')
			{
				type = !type;
				continue;
			}
			else if ((buffer[i]) == '*')
			{
				break;
			}
			
			if (type)
			{
				messageType += buffer[i];
			}
			else
			{
				message += buffer[i];
			}
		}

		int code = std::stoi(messageType);

		NetworkDataHandler* handler = getDataHandler(code);
		if (handler != nullptr)
		{
			handler->handleData(message, nullptr);
		}
		else
		{
			std::cout << "Unknown Message with ID: " << messageType << "\n";
		}
	}
}

int NetworkServer::receiveData(unsigned int client_id)
{
	if (sessions.find(client_id) != sessions.end())
	{
		SOCKET currentSocket = sessions[client_id];
		return recv(currentSocket, buffer, BUFFER_SIZE, 0);
	}

	return 0;
}
