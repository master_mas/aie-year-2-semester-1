#include "INetworkBase.h"

void INetworkBase::registerDataHandler(NetworkDataHandler * handler)
{
	for (unsigned int i = 0; i < networkDataHandlersList.size(); ++i)
	{
		if (networkDataHandlersList[i]->getDataType() == handler->getDataType())
		{
			return;
		}
	}

	networkDataHandlersList.push_back(handler);
}

NetworkDataHandler * INetworkBase::getDataHandler(int messageType)
{
	for (unsigned int i = 0; i < networkDataHandlersList.size(); ++i)
	{
		if (networkDataHandlersList[i]->getDataType() == messageType)
		{
			return networkDataHandlersList[i];
		}
	}

	return nullptr;
}
