#pragma once

#include <vector>
#include "NetworkDataHandler.h"
#include "NetworkGenericType.h"
#include "Socket.h"
#include <thread>

class NetworkDataListener
{
public:
	const int BUFFER_SIZE = 256;
	const int NETWORK_RECIEVING_QUEUE_TIMEOUT = 100;

	NetworkDataListener(std::vector<NetworkDataHandler*>* networkHandlers, Socket* socket, std::vector<Address*>* currentlyConnected, char* topLevelAddressMeta);
	~NetworkDataListener();

	void notifyHandlerUpdate();
	void recieveData();

	void setRunning(bool running);
private:
	std::vector<int> referenceToHandlers;
	std::vector<NetworkDataHandler*>* networkHandlers = nullptr;
	std::vector<Address*>* currentlyConnected = nullptr;
	Socket* socket = nullptr;

	char* topLevelAddressMeta = nullptr;
	bool isRunning = true;

	bool checkPendingNetworkQueues(Address * listen);
};

