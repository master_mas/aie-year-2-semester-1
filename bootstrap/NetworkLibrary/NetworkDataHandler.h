#pragma once
#include "Address.h"
#include <string>
class NetworkDataHandler
{
public:
	NetworkDataHandler(int dataType);
	~NetworkDataHandler();

	virtual void handleData(std::string data, Address* address) = 0;

	int getDataType() const;

private:
	int dataType = -1;
};