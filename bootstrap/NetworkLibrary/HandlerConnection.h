#pragma once

#include "NetworkDataHandler.h"
#include <vector>

class HandlerConnection :
	public NetworkDataHandler
{
public:
	HandlerConnection(std::vector<Address*>* currentlyConnected);
	~HandlerConnection();

private:
	std::vector<Address*>* currentlyConnected = nullptr;

	// Inherited via NetworkDataHandler
	virtual void handleData(std::string data, Address * address) override;
};

