#include "NetworkClient.h"

NetworkClient::NetworkClient()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSADATA WsaData;
	WSAStartup(MAKEWORD(2, 2), &WsaData) == NO_ERROR;
#else
	return true;
#endif
	
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;  //TCP connection!!!

	std::cout << "Please enter a ip:\n";
	std::string ip;
	std::getline(std::cin, ip);

	iResult = getaddrinfo(ip.c_str(), "30000", &hints, &result);

	if (iResult != 0)
	{
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		exit(1);
	}

	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);

		if (client == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			exit(1);
		}

		// Connect to server.
		iResult = connect(client, ptr->ai_addr, (int)ptr->ai_addrlen);

		if (iResult == SOCKET_ERROR)
		{
			closesocket(client);
			client = INVALID_SOCKET;
			printf("The server is down... did not connect");
		}
	}

	freeaddrinfo(result);

	if (client == INVALID_SOCKET)
	{
		printf("Unable to connect to server!\n");
		WSACleanup();
		exit(1);
	}

	u_long iMode = 1;

	iResult = ioctlsocket(client, FIONBIO, &iMode);
	if (iResult == SOCKET_ERROR)
	{
		printf("ioctlsocket failed with error: %d\n", WSAGetLastError());
		closesocket(client);
		WSACleanup();
		exit(1);
	}

	char value = 1;
	setsockopt(client, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));

	sendData("Hello", 1);
	std::thread(getMessage);
}


NetworkClient::~NetworkClient()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSACleanup();
#endif
}

void NetworkClient::sendData(const char * data, int messageType)
{
	std::string prefixMessage = "&";
	std::string code = std::to_string(messageType);
	std::string subfixMessage = "&";
	std::string construct = std::string(data);
	std::string endMessage = "*";

	prefixMessage.append(code);
	prefixMessage.append(subfixMessage);
	prefixMessage.append(construct);
	prefixMessage.append(endMessage);

	const char* buf = prefixMessage.c_str();
	send(client, buf, BUFFER_SIZE, 0);
}

void NetworkClient::getMessage()
{
	int length = receivePackets();

	if (length <= 0)
	{
		return;
	}

	std::cout << buffer << std::endl;
}

void NetworkClient::update()
{
	std::string	input;
	std::string code;

	std::cout << "Message Type (Number):\n";
	std::getline(std::cin, code);
	std::cout << "Input:\n";
	std::getline(std::cin, input);

	int messageType = stoi(code);
	sendData(input.c_str(), messageType);

	/*if (!isExit)
	{
		std::string message;
		std::string messageType;
		bool messageTypeReader = false;

		do
		{
			recv(client, buffer, INetworkBase::BUFFER_SIZE, 0);
			if (*buffer == '#')
			{
				*buffer = '*';
				isExit = true;
			}
			else if (*buffer == '&')
			{
				messageTypeReader = !messageTypeReader;
			}
			else
			{
				if (messageTypeReader)
				{
					messageType.append(buffer);
				}
				else
				{
					message.append(buffer);
				}
			}
		} while (*buffer == 42);

		if (!messageType.empty())
		{
			NetworkDataHandler* handler = getDataHandler(stoi(messageType));
			if (handler != nullptr)
			{
				handler->handleData(message, nullptr);
			}
			else
			{
				std::cout << "Unknown Message Type: " << messageType << std::endl;
			}
		}
	}*/
}

int NetworkClient::receivePackets()
{
	return recv(client, buffer, BUFFER_SIZE, 0);
}
