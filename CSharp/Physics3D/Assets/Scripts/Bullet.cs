﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour {

    private Rigidbody rigidBody;
    public float force = 1.0f;

	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.AddForce(transform.forward * force * Time.fixedDeltaTime, ForceMode.Impulse);
        Destroy(this.gameObject, 5);
    }

    void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }
}
