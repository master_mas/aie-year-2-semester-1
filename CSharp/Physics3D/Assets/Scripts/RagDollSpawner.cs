﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagDollSpawner : MonoBehaviour {

    public GameObject ragDoll;
    public float waitTime = 7;
    private float timer = 0;

	void Start () {
        timer = waitTime;
	}
	
	void Update () {
        timer += Time.deltaTime;
        if(timer >= waitTime)
        {
            Instantiate(ragDoll, this.transform.position, Quaternion.Euler(0, Random.Range(0, 360), 0));
            timer = 0;
        }
	}
}
