﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Play : MonoBehaviour {

    private Camera camera1;
    public GameObject enable;

	void Start () {
        camera1 = GetComponent<Camera>();
	}
	
	void Update () {
		if(Input.GetMouseButtonDown(0))
        {
            Ray ray = camera1.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            //Debug.DrawRay(Input.mousePosition, new Vector3(0,0,1));
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag.Equals("Play"))
                {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().processInput = true;
                    hit.transform.parent.gameObject.SetActive(false);
                    enable.SetActive(true);
                }
            }
        }
	}
}
