﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaLighting : MonoBehaviour {

    public GameObject light;
    private ParticleSystem particleSystem;

	void Start () {
        particleSystem = GetComponent<ParticleSystem>();
        GameObject obj = Instantiate(light, transform.position, Quaternion.identity);

        Destroy(this.gameObject, 5);
        Destroy(obj, 3);

        //obj.transform.position = Vector3.zero;
	}
}
