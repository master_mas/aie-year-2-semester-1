﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rigidBody;
    private Camera camera;

    [HideInInspector]
    public bool processInput = false;

    [Range(0.1f, 1000.0f)]
    public float forwardForce = 1.0f;
    [Range(1.0f, 100.0f)]
    public float angularForce = 1.0f;

    public GameObject bullet;
    public GameObject spawnPoint;
    public float spawnRate = 1.0f;
    private float timer;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        timer = spawnRate;
    }

    void FixedUpdate()
    {
        if (!processInput)
            return;

        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");

        if (hAxis < 0)
        {
            transform.Rotate(transform.up, -angularForce * Time.fixedDeltaTime);
        }
        else if (hAxis > 0)
        {
            transform.Rotate(transform.up, angularForce * Time.fixedDeltaTime);
        }

        if (vAxis < 0)
        {
            rigidBody.AddForce(transform.forward * -forwardForce * Time.fixedDeltaTime, ForceMode.Impulse);
        }
        else if (vAxis > 0)
        {
            rigidBody.AddForce(transform.forward * forwardForce * Time.fixedDeltaTime, ForceMode.Impulse);
        }

        
    }

    void Update()
    {
        if (!processInput)
            return;

        if(Input.GetMouseButton(0) && timer >= spawnRate)
        {
            timer = 0;

            //Ray ray = camera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            //Ray ray = camera.ViewportPointToRay(Vector3.zero);
            //current = ray.origin;

            //RaycastHit hit;
            //Physics.Raycast(ray, out hit);

            //Vector3 difference = hit.transform.position - spawnPoint.transform.position;
            //float sign = (spawnPoint.transform.position.y < hit.transform.position.y) ? -1.0f : 1.0f;
            //float angle = Vector3.Angle(Vector3.right, difference) * sign;

            //Quaternion rotation = new Quaternion();
            //rotation.SetLookRotation(spawnPoint.transform.parent.parent.forward * angle);

            Instantiate(bullet, spawnPoint.transform.position, spawnPoint.transform.parent.rotation);
        }

        timer += Time.deltaTime;

        if (Input.GetKey(KeyCode.Q))
        {
            print("Hi");
            spawnPoint.transform.Rotate(Vector3.up, 20 * Time.fixedDeltaTime);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            print("Hello");
            spawnPoint.transform.Rotate(Vector3.up, -20 * Time.fixedDeltaTime);
        }
    }
}
