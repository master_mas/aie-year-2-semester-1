﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakSign : MonoBehaviour {

    public float waitTime = 0;
    private float timer = 0;

    public SpringJoint joint;

	void Start () {
		
	}
	
	void Update () {
        timer += Time.deltaTime;

		if(timer >= waitTime)
        {
            Destroy(joint);
            Destroy(this);
        }
	}
}
