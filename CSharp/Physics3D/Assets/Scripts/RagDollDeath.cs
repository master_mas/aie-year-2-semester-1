﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagDollDeath : MonoBehaviour {

    public GameObject deathPlay;

    private bool bypass = false;

    void OnCollisionEnter(Collision collision)
    {
        if (bypass)
            return;

        if(collision.transform.tag.Equals("Bullet"))
        {
            GameObject obj = Instantiate(deathPlay, collision.contacts[0].point, Quaternion.identity);
            obj.GetComponent<ParticleSystem>().collision.SetPlane(0, GameObject.FindGameObjectWithTag("Base").transform);
            Destroy(this.gameObject, 3);
            Ragdoll ragdoll = GetComponentInParent<Ragdoll>();
            if (ragdoll != null)
                ragdoll.RagdollOn = true;
            Destroy(GetComponentInParent<CapsuleCollider>());
            Destroy(this);
            bypass = true;
        }
    }
}
