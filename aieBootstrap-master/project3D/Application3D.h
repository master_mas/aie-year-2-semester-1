#pragma once

#include <gl_core_4_4.h>
#include "Application.h"
#include "FlyCamera.h"
#include <glm/mat4x4.hpp>
#include "Shader.h"
//#include "Model.h"
#include "Texture2D.h"

class Application3D : public aie::Application {
public:

	Application3D();
	virtual ~Application3D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:
	Camera* m_camera = nullptr;
	//Model* m_mesh = nullptr;
	Shader* m_shader = nullptr;
	Texture2D* texture = nullptr;
};