#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "Shader.h"
#include "Mesh.h"

#include <Importer.hpp>
#include <scene.h>
#include <postprocess.h>

#include <stb_image.h>

GLint TextureFromFile(const char* path, std::string directory);

class Model
{
public:
	Model(const char* path);
	~Model();

	void draw(Shader shader);

private:
	std::vector<Mesh> meshes;
	std::string directory;
	std::vector<Structure::Texture> textures_loaded;

	void loadModel(std::string path);
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Structure::Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
};

