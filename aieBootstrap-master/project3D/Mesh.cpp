#include "Mesh.h"
#include <iostream>

Mesh::Mesh(std::vector<Structure::Vertex> vertices, std::vector<GLuint> indices, std::vector<Structure::Texture> textures)
{
	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;

	this->setupMesh();
}

Mesh::~Mesh()
{
}


void Mesh::setupMesh()
{
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Structure::Vertex), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint),
		&this->indices[0], GL_STATIC_DRAW);

	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex),
		(GLvoid*)0);
	// Vertex Normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), (void*)(sizeof(glm::vec4)));
	// Vertex Texture Coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), (void*)(sizeof(glm::vec4) + sizeof(glm::vec3)));

	glBindVertexArray(0);
}

void Mesh::draw(Shader shader)
{
	GLuint diffuseNr = 1;
	GLuint specularNr = 1;
	for (GLuint i = 0; i < this->textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i); // Activate proper texture unit before binding
										  // Retrieve texture number (the N in diffuse_textureN)
		std::stringstream ss;
		std::string number;
		std::string name = this->textures[i].type;
		if (name == "texture_diffuse")
			ss << diffuseNr++; // Transfer GLuint to stream
		else if (name == "texture_specular")
			ss << specularNr++; // Transfer GLuint to stream
		number = ss.str();

		glUniform1f(glGetUniformLocation(shader.getProgramID(), ("material." + name + number).c_str()), i);
		glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
	}
	glActiveTexture(GL_TEXTURE0);

	// Draw mesh
	glBindVertexArray(this->VAO);
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

//
//void Mesh::generateGrid(unsigned int rows, unsigned int cols, int offsetX, int offsetY, int offsetZ)
//{
//	m_cols = cols;
//	m_rows = rows;
//	Structure::Vertex* aoVertices = new Structure::Vertex[rows * cols];
//	for (unsigned int r = 0; r < rows; ++r)
//	{
//		for (unsigned int c = 0; c < cols; ++c)
//		{
//			aoVertices[r * cols + c].position = glm::vec4((float)c + offsetX, 0 + offsetY, (float)r + offsetZ, 1);
//			aoVertices[r * cols + c].texCoord = glm::vec2(r, c);
//			glm::vec3 colour = glm::vec3(-sinf((c / (float)(cols - 1)) * -(r / (float)(rows - 1))), -sinf((c / (float)(cols - 1)) * (r / (float)(rows - 1))), sinf((c / (float)(cols - 1)) * (r / (float)(rows - 1))));
//			aoVertices[r * cols + c].colour = glm::vec4(colour, 1);
//		}
//	}
//
//	unsigned int* auiIndices = new unsigned int[(rows - 1) * (cols - 1) * 6];
//	unsigned int index = 0;
//	for (unsigned int r = 0; r < (rows - 1); ++r)
//	{
//		for (unsigned int c = 0; c < (cols - 1); ++c)
//		{
//			// triangle 1
//			auiIndices[index++] = r * cols + c;
//			auiIndices[index++] = (r + 1) * cols + c;
//			auiIndices[index++] = (r + 1) * cols + (c + 1);
//			// triangle 2 
//			auiIndices[index++] = r * cols + c;
//			auiIndices[index++] = (r + 1) * cols + (c + 1);
//			auiIndices[index++] = r * cols + (c + 1);
//		}
//	}
//
//	glGenBuffers(1, &m_VBO);
//	glGenBuffers(1, &m_IBO);
//
//	glGenVertexArrays(1, &m_VAO);
//	glBindVertexArray(m_VAO);
//
//	//VBO
//	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
//	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(Structure::Vertex), aoVertices, GL_STATIC_DRAW);
//
//	glEnableVertexAttribArray(0);
//	glEnableVertexAttribArray(1);
//	glEnableVertexAttribArray(2);
//	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), 0);
//	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), (void*)(sizeof(glm::vec4)));
//	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Structure::Vertex), (void*)(sizeof(glm::vec4) * 2));
//
//	//IBO
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(GLuint), auiIndices, GL_STATIC_DRAW);
//
//	glBindVertexArray(0);
//
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//
//	delete[] aoVertices;
//	delete[] auiIndices;
//}
//
//void Mesh::loadMesh(const char * fileName)
//{
//	
//}
//
//void Mesh::generateBuffers()
//{
//	glGenBuffers(1, &m_VBO);
//	glGenBuffers(1, &m_IBO);
//
//	glGenVertexArrays(1, &m_VAO);
//	glBindVertexArray(m_VAO);
//}
//
//Mesh::Mesh(std::vector<Structure::Vertex> vertices, std::vector<GLuint> indices, std::vector<Structure::Texture> textures)
//{
//}
//
//unsigned int Mesh::getRows() const
//{
//	return m_rows;
//}
//
//unsigned int Mesh::getCols() const
//{
//	return m_cols;
//}
