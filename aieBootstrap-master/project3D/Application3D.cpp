#include "Application3D.h"
#include "Gizmos.h"
#include "Input.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <string>

Application3D::Application3D() 
{

}

Application3D::~Application3D() 
{

}


unsigned int indexData[] = { 0, 1, 2, 0, 2, 3, };

bool Application3D::startup() 
{
	m_camera = new FlyCamera();
	m_camera->setPerspective(glm::pi<float>() * 0.25f, (float)getWindowWidth() / (float)getWindowHeight(), 0.1f, 1000.f);
	m_camera->setLookAt(glm::vec3(8, 3, -5), glm::vec3(0), glm::vec3(0, 1, 0));
	m_camera->setPosition(glm::vec3(10, 3, 0));

	setBackgroundColour(0.0f, 0.0f, 0.0f);

	texture = new Texture2D();
	texture->loadTexture("textures/rock_medium.png");

	m_shader = new Shader();
	m_shader->loadShader("../project3D/default.vert", "../project3D/default.frag");

	//m_mesh = new Model("models/Bunny.obj");
	/*m_mesh->generateGrid(15, 15, 0, 0, 0);

	m_mesh2 = new Mesh();
	m_mesh2->generateGrid(15, 15, 1, 1, 1);*/

	// initialise gizmo primitive counts
	aie::Gizmos::create(10000, 10000, 10000, 10000);

	return true;
}

void Application3D::shutdown() 
{
	delete m_camera;
	//delete m_mesh;
	aie::Gizmos::destroy();
}

void Application3D::update(float deltaTime) 
{
	float time = getTime();

	aie::Gizmos::clear();

	m_camera->update(deltaTime, m_window);

	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Application3D::draw() 
{
	clearScreen();
	
	//glUseProgram(m_shader->getProgramID());

	//int loc = glGetUniformLocation(m_shader->getProgramID(), "projectionViewWorldMatrix");
	//glUniformMatrix4fv(loc, 1, GL_FALSE, &(m_camera->getProjectionView()[0][0]));

	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, texture->getTexture());

	//loc = glGetUniformLocation(m_shader->getProgramID(), "diffuse");
	//glUniform1i(loc, 0);

	//m_mesh->draw(*m_shader);

	/*glBindVertexArray(m_mesh->m_VAO);
	unsigned int indexCount = (m_mesh->getRows() - 1) * (m_mesh->getCols() - 1) * 6;
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, nullptr);*/

	//glBindVertexArray(m_mesh2->m_VAO);
	//indexCount = (m_mesh2->getRows() - 1) * (m_mesh2->getCols() - 1) * 6;
	//glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, nullptr);

	//Wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//aie::Gizmos::draw(camera->getProjectionView());
}
