#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>

#include "Structure.h"
#include "Importer.hpp"
#include "Shader.h"

#include <vector>
#include <sstream>

class Mesh
{
public:
	std::vector<Structure::Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Structure::Texture> textures;

	Mesh(std::vector<Structure::Vertex> vertices, std::vector<GLuint> indices, std::vector<Structure::Texture> textures);
	~Mesh();
	void draw(Shader shader);

	//unsigned int m_VAO;
	//unsigned int m_VBO;
	//unsigned int m_IBO;
	
	//void generateGrid(unsigned int rows, unsigned int cols, int offsetX, int offsetY, int offsetZ);
	
private:
	GLuint VAO, VBO, EBO;

	void setupMesh();
};

