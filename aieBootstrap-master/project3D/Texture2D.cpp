#include "Texture2D.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>
//#include <stb_image.h>

Texture2D::Texture2D()
{
}

Texture2D::~Texture2D()
{
}

void Texture2D::loadTexture(const char* fileName)
{
	m_data = stbi_load(fileName, &m_width, &m_height, &m_fileType, STBI_default);

	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	stbi_image_free(m_data);
}

unsigned int Texture2D::getTexture()
{
	return m_texture;
}
