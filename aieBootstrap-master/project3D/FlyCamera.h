#pragma once
#include "Camera.h"
class FlyCamera :
	public Camera
{
public:
	FlyCamera();
	~FlyCamera();

	void update(float deltaTime, GLFWwindow* window);
	void setSpeed(float speed);
	void rotateCameraUsingKeys(float delta, GLFWwindow* window);
	void moveCameraUsingKeys(float delta, GLFWwindow* window);

private:
	float m_rotateSpeed = .01f;
	float m_moveSpeed = 5;
	vec3 up = vec3(0, 1, 0);
};

