#pragma once
#include <glm/glm.hpp>
#include <string>

class Structure
{
public:
	Structure()
	{

	}

	~Structure()
	{

	}

	struct Vertex {
		glm::vec4 position;
		glm::vec3 normal;
		glm::vec2 texCoord;
	};

	struct Texture
	{
		GLuint id;
		std::string type;
		std::string path;
	};
};

