#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>
#include <fstream>
#include <iostream>

class Shader
{
public:
	Shader();
	~Shader();

	void loadShader(const char* vertPath, const char* fragPath);
	unsigned int getProgramID();

private:
	unsigned int m_programID = 0;
	std::string readFile(const char* filePath);
};

