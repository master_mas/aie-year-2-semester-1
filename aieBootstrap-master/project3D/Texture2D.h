#pragma once

//#define STB_IMAGE_IMPLEMENTATION 
#include <stb_image.h>

class Texture2D
{
public:
	Texture2D();
	~Texture2D();

	void loadTexture(const char*);
	unsigned int getTexture();
private:
	int m_width = 0;
	int m_height = 0;

	int m_fileType = 0;

	unsigned char* m_data = nullptr;
	unsigned int m_texture = 0;
};