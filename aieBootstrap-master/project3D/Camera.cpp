#include "Camera.h"

Camera::Camera()
{
	worldTransform = mat4();
	viewTransform = mat4();
	projectionTransform = mat4();
	projectionViewTransform = mat4();
}

Camera::~Camera()
{
}

void Camera::setPerspective(float fieldOfView, float aspectRatio, float near, float far)
{
	projectionTransform = mat4(
		1.0f / (aspectRatio * tan(fieldOfView / 2)), 0.0f, 0.0f, 0.0f, 
		0.0f, 1.0f / (tan(fieldOfView / 2)), 0.0f, 0.0f,
		0.0f, 0.0f, -((far + near) / (far - near)), -1.0f,
		0.0f, 0.0f, -((2 * (far * near)) / (far - near)), 0.0f);

	//projectionTransform = glm::perspective(fieldOfView, aspectRatio, near, far);
}

void Camera::setLookAt(vec3 from, vec3 to, vec3 up)
{
	viewTransform = glm::lookAt(from, to, up);
	updateWorldTransform();
	updateProjectionView();
}

void Camera::setPosition(const vec3 & position)
{
	//worldTransfrom[3] = vec4(position, 1);

	worldTransform[3][0] = position.x;
	worldTransform[3][1] = position.y;
	worldTransform[3][2] = position.z;
	updateViewTransform();
	updateProjectionView();
	//updateWorldTransfrom();
}

mat4 & Camera::getWorldTransform()
{
	return worldTransform;
}

mat4 & Camera::getView()
{
	return viewTransform;
}

mat4 & Camera::getProjection()
{
	return projectionTransform;
}

mat4 & Camera::getProjectionView()
{
	return projectionViewTransform;
}

void Camera::setWorldTransform(const glm::mat4& transform)
{
	worldTransform = transform;
	viewTransform = glm::inverse(worldTransform);
	updateProjectionView();
	updateWorldTransform();
}

vec3 Camera::getPosition()
{
	return getRow(3);
}

vec3 Camera::getRow(int row)
{
	return vec3(worldTransform[row].x, worldTransform[row].y, worldTransform[row].z);
}

void Camera::updateProjectionView()
{
	projectionViewTransform = projectionTransform * viewTransform;
}

void Camera::updateWorldTransform()
{
	worldTransform = glm::inverse(viewTransform);
}

void Camera::updateViewTransform()
{
	viewTransform = glm::inverse(worldTransform);
}
