#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <imgui_glfw3.h>
#include "..\dependencies\glfw\include\GLFW\glfw3.h"

using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

class Camera
{
public:
	Camera();
	virtual ~Camera();

	virtual void update(float deltaTime, GLFWwindow* window) = 0;
	void setPerspective(float fieldOfView, float aspectRatio, float near, float far);
	void setLookAt(vec3 from, vec3 to, vec3 up);
	void setPosition(const vec3 & position);
	mat4& getWorldTransform();
	mat4& getView();
	mat4& getProjection();
	mat4& getProjectionView();

	void setWorldTransform(const glm::mat4& transform);

	vec3 getPosition();
	vec3 getRow(int row);

private:
	mat4 worldTransform;
	mat4 viewTransform;
	mat4 projectionTransform;
	mat4 projectionViewTransform;

	void updateProjectionView();
	void updateWorldTransform();
	void updateViewTransform();
};

